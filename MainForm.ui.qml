import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "RectAutoSize.js" as RectAutoSizeScript

/*!
  \qmltype MainForm

  Jest to klasa reprezentująca GUI. Scala ze sobą i zarządza pomniejszymi
  elementami interfejsu użytkownika.
  */
Rectangle {
    property alias mainForm: mainForm
    property alias menu: menu
    property alias cellValues: automatonControls.cellValues
    property alias automatonContainer: automatonContainer
    property alias automatonControls: automatonControls
    property alias files: files
    property alias authors: authors
    property alias charTable: charTable
    property alias scriptEditor: scriptEditor
    property alias scriptContainer: scriptContainer

    id: mainForm
    anchors.fill: parent
    color: "#172227"

    width: 1280
    height: 720

    // -> POMNIEJSZE CZĘŚCI INTERFEJSU
    Files {
        id: files
    }

    MenuBar {
        id: menu
    }

    Logo {
    }

    AutomatonContainer {
        id: automatonContainer
        anchors.right: automatonControls.left
        anchors.rightMargin: 40
    }

    AutomatonControls {
        id: automatonControls
    }

    WelcomeScreen {
    }

    WindowAuthors {
        id: authors
    }

    WindowCharTable {
        id: charTable
    }

    ScriptEditor {
        id: scriptEditor
    }

    Rectangle {
        visible: false
        id: scriptContainer
    }
    // <- POMNIEJSZE CZĘŚCI INTERFEJSU
}
