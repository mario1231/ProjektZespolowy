import QtQuick 2.8
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "custom-controls/"

import "Files.js" as FilesScript

/*!
  \qmltype ScriptEditor

  Jest to okno służące do edycji aktualnie wczytanego skryptu.
  */
Rectangle {
    property alias editingArea: editingArea

    anchors.fill: parent
    color: "#88000000"
    width: 640
    height: 480
    visible: false

    // -> OKNO
    Rectangle {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "#f6f6f6"
        anchors.rightMargin: 50
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        anchors.topMargin: 50

        // -> OKNO - TYTUŁ
        Text {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.leftMargin: 10
            width: 200
            height: 20
            verticalAlignment: Text.AlignVCenter
            text: "Edycja skryptu"
        }
        // -> OKNO - TYTUŁ


        // -> OKNO - PRZYCISK ZAMYKAJĄCY
        Image {
            id: closeButton
            width: 20
            height: 20
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 10
            source: "img/close.png"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    scriptEditor.visible = false;
                    FilesScript.runLoadedScript(automatonContainer.getAutomatonScript().url);
                    automatonContainer.start();
                }
            }
        }
        // <- OKNO - PRZYCISK ZAMYKAJĄCY


        // -> OKNO - POLE TEKSTOWE EDYCJI SKRYPTU
        ScrollView {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: closeButton.bottom
            anchors.bottom: saveButton.top
            anchors.topMargin: 10
            anchors.bottomMargin: 10

            TextArea {
                id: editingArea
                color: "#fff"
                font.pixelSize: 20
                font.family: "Calibri"
                padding: 10

                background: Rectangle {
                    color: "#333"
                }
            }
        }
        // <- OKNO - POLE TEKSTOWE EDYCJI SKRYPTU


        // -> OKNO - PRZYCISK ZAPISU
        AppButton {
            id: saveButton
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.bottomMargin: 10
            anchors.rightMargin: 10
            text: "Zapisz"

            onClicked: {
                FilesScript.saveFile(automatonContainer.getAutomatonScript().url, editingArea.text);
            }
        }
        // <- OKNO - PRZYCISK ZAPISU
    }
    // <- OKNO
}
