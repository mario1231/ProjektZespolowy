/*!
  \fn RectAutoSize.rectAutoSize(rect : Rectangle) : void

  Funkcja ta zmienia wysokość \a rect tak, by była równa jego szerokości.
  */
function rectAutoSize(rect) {
    rect.width = rect.height;
}
