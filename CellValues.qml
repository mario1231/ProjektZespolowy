import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

/*!
  \qmltype CellValues

  \brief Tabela służąca do edycji i dodawania oraz usuwania parametrów komórek.

  Klasa CellValues jest graficznym interfejsem służącym do wyświetlania,
  edycji dodawania i usuwania parametrów komórki.
  Kolumny tabeli reprezentują parametry, wiersze zaś wartości parametrów.
  Jedna wartość może być przypisana do dowolnej liczby parametrów.
  Parametry nieokreślone wyświetlane są w ostatnim rzędzie tabeli, który
  reprezentuje domyślną wartość parametrów.
  */
ColumnLayout {
    id: cellValues

    property alias valuesColumn: valuesColumn
    property alias nullValue: nullValue

    //ButtonGropu'y grupujące RadioBox'y. RadioBox'y są grupowane pionowo.
    property alias rGroup: rGroup
    property alias gGroup: gGroup
    property alias bGroup: bGroup
    property alias borderGroup: borderGroup
    property alias charGroup: charGroup
    property alias stateGroup: stateGroup
    property alias addGroup: addGroup

    /*!
      \fn CellValues.updateValues(void) : void

      Funkcja auktualizująca wszystkie parametry komórek.
      */
    function updateValues() {
        if (automatonContainer === undefined || automatonContainer.getAutomatonGraphics() === undefined)
            return;

        if(automatonContainer.getAutomatonGraphics().getSelectedCell() === undefined ||
            automatonContainer.getAutomatonGraphics().getSelectedCell() === null)
            return;

        for (var i = 0; i < cellValues.valuesColumn.children.length; ++i)
            cellValues.valuesColumn.children[i].updateValue(false);

        nullValue.updateValue(true);
    }

    /*!
      \fn CellValue.addParam(void) : void

      Funkcja dodająca wiersz umożliwiający dodanie i edycję nowego parametru komórki.
      */
    function addParam() {
        var object = Qt.createComponent("CellValue.qml");
        object.createObject(valuesColumn);
    }

    // -> GRUPY PORZADKUJĄCE RADIOBOXY WYBORU PARAMETRÓW
    ButtonGroup {
        id: rGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: gGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: bGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: borderGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: charGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: stateGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    ButtonGroup {
        id: addGroup
        onCheckedButtonChanged: {
            updateValues();
        }
    }
    // <- GRUPY PORZADKUJĄCE RADIOBOXY WYBORU PARAMETRÓW


    // -> PASEK TYTUŁOWY TABELI PARAMETRÓW
    Row {
        id: row
        spacing: 5

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "R"
                color: "#f00"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "G"
                color: "#0f0"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "B"
                color: "#00f"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "Ram."
                color: "#fff"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "Znak"
                color: "#fff"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "Stan"
                color: "#fff"
            }
        }

        Rectangle {
            width: 30
            height: 30
            color: "#00000000"

            Text {
                anchors.centerIn: parent
                text: "Dod."
                color: "#fff"
            }
        }
    }
    // <- PASEK TYTUŁOWY TABELI PARAMETRÓW


    // -> TABELA PARAMETRÓW
    Column {
        id: valuesColumn
        Layout.fillWidth: true

        spacing: 5
    }


    // -> TABELA PARAMETRÓW - PRZYCISK DODAJĄCY NOWY WIERSZ PARAMETRU
    Rectangle {
        Layout.fillWidth: true
        height: 20
        color: "#1ba08e"

        Text {
            text: "Dodaj parametr"
            color: "#fff"
            font.pixelSize: 10
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: addParam();
        }
    }
    // <- TABELA PARAMETRÓW - PRZYCISK DODAJĄCY NOWY WIERSZ PARAMETRU


    // -> TABELA PARAMETRÓW - WIERSZ DOMYŚLNEJ WARTOŚCI PARAMETRÓW
    CellValue {
        id: nullValue

        Component.onCompleted: selectAllRadios()
    }
    // <- TABELA PARAMETRÓW - WIERSZ DOMYŚLNEJ WARTOŚCI PARAMETRÓW
}
