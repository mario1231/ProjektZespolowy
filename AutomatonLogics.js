﻿//Instancja obiektu odpowiedzialnego za logikę automatu
var automatonLogics;

/*!
    \fn AutomatonLogics.createAutomatonLogics(automaton : Automaton) : void

    Funkcja tworzy obiekt automatu odpowiedzialny za logikę automatu.
  */
function createAutomatonLogics(automaton) {
    automatonLogics = new AutomatonLogics(automaton);
}

/*!
  \qmltype AutomatonLogics

    Klasa AutomatonLogics reprezentuje interfejs obsługujący logikę \a automatu.
    Udostępnia metody służące do manipulacji wartościami sądziedztwa,
    periodyczności, pobierania sąsiadów komórki oraz pobierania komórki.
  */
function AutomatonLogics(automaton) {
    this.automaton = automaton;

    this.NEUMANN = 4;
    this.MOORE = 8;
    this.nullNeighbourValue = this.automaton.nullCell;

    this.isPeriodic = false;
    this.neighbourCount = this.NEUMANN;
}

/*!
  \fn AutomatonLogics.setPeriodicity(isPeriodic : bool) : void

  Funkcja ustalająca czy automat ma być periodyczny.
  Gdy \a isPeriodic = true automat jest periodyczny.
  */
AutomatonLogics.prototype.setPeriodicity = function(isPeriodic) {
    this.isPeriodic = isPeriodic;
}

/*!
  \fn AutomatonLogics.setNeighbourhood(count : int) : void

  W zależności od wartości \a count (4 lub 8) funkcja ustawia typ sąsiedztwa automatu:
  dla wartości 4 ustawia sądziedztwo Neunama, dla wartości 8 zaś - Moore'a
  */
AutomatonLogics.prototype.setNeighbourhood = function(count) {
    if (this.NEUMANN === count)
        this.neighbourCount = this.NEUMANN;
    else if (this.MOORE === count)
        this.neighbourCount = this.MOORE;
}

/*!
    \fn AutomatonLogics.getNeighbour(i : int, j : int, direction : String) : Cell

    Funkcja pobiera sąsiada komórki o współrzędnych \a i, \a j, który
    znajduje się w kierunku \a direction od komórki.
    W przypadku braku sąsiada zwracana jest komórka o domyślnych parametrach.
  */
AutomatonLogics.prototype.getNeighbour = function(i, j, direction) {
    if (this.isPeriodic) {
        switch (direction.toLowerCase()) {
            case "n": return (i > 1) ?
                this.automaton.cells[i-1][j] : this.automaton.cells[this.automaton.n - 1][j];
            case "ne": return (i > 1 && j < this.automaton.n - 1) ?
                this.automaton.cells[i-1][j+1] : this.automaton.cells[this.automaton.n - 1][0];
            case "e": return (j < this.automaton.n - 1) ?
                this.automaton.cells[i][j+1] : this.automaton.cells[i][0];
            case "se": return (i < this.automaton.n - 1 && j < this.automaton.n - 1) ?
                this.automaton.cells[i+1][j+1] : this.automaton.cells[0][0];
            case "s": return (i < this.automaton.n - 1) ?
                this.automaton.cells[i+1][j] : this.automaton.cells[0][j];
            case "sw": return (i < this.automaton.n - 1 && j > 1) ?
                this.automaton.cells[i+1][j-1] : this.automaton.cells[0][this.automaton.n - 1];
            case "w": return (j > 1) ?
                this.automaton.cells[i][j-1] : this.automaton.cells[i][this.automaton.n - 1];
            case "nw": return (i > 1 && j > 1) ?
                this.automaton.cells[i-1][j-1] : this.automaton.cells[this.automaton.n - 1][this.automaton.n - 1];

            default:
                return this.nullNeighbourValue;
        }
    }
    else {
        switch (direction.toLowerCase()) {
            case "n": return (i > 1) ?
                this.automaton.cells[i-1][j] : this.nullNeighbourValue;
            case "ne": return (i > 1 && j < this.automaton.n - 1) ?
                this.automaton.cells[i-1][j+1] : this.nullNeighbourValue;
            case "e": return (j < this.automaton.n - 1) ?
                this.automaton.cells[i][j+1] : this.nullNeighbourValue;
            case "se": return (i < this.automaton.n - 1 && j < this.automaton.n - 1) ?
                this.automaton.cells[i+1][j+1] : this.nullNeighbourValue;
            case "s": return (i < this.automaton.n - 1) ?
                this.automaton.cells[i+1][j] : this.nullNeighbourValue;
            case "sw": return (i < this.automaton.n - 1 && j > 1) ?
                this.automaton.cells[i+1][j-1] : this.nullNeighbourValue;
            case "w": return (j > 1) ?
                this.automaton.cells[i][j-1] : this.nullNeighbourValue;
            case "nw": return (i > 1 && j > 1) ?
                this.automaton.cells[i-1][j-1] : this.nullNeighbourValue;

            default:
                return this.nullNeighbourValue;
        }
    }
}

/*!
    \fn AutomatonLogics.getNeighBours(i : int, j : int) : Cells[]

    Funkcja zwracająca wszystkich sąsiadów komórki o współrzędnych \a i, \a j.
  */
AutomatonLogics.prototype.getNeighbours = function(i, j) {
    if (this.neighbourCount === this.MOORE) {
        return [
            this.getNeighbour(i, j, "n"),
            this.getNeighbour(i, j, "ne"),
            this.getNeighbour(i, j, "e"),
            this.getNeighbour(i, j, "se"),
            this.getNeighbour(i, j, "s"),
            this.getNeighbour(i, j, "sw"),
            this.getNeighbour(i, j, "w"),
            this.getNeighbour(i, j, "nw")
        ];
    } else {
        return [
            this.getNeighbour(i, j, "n"),
            this.getNeighbour(i, j, "e"),
            this.getNeighbour(i, j, "s"),
            this.getNeighbour(i, j, "w")
        ];
    }
}

/*!
  \fn AutomatonLogics.getCell(i : int, j : int) : Cell

  Funkcja zwracająca komórkę o wzpółrzędnych \a i, \a j.
  */
AutomatonLogics.prototype.getCell = function(i, j) {
    return this.automaton.cells[i][j];
}

