import QtQuick 2.8

/*!
    \qmltype AutomatonCanvas

    Płótno w którym rysowane są komórki automatu.
*/
Canvas {
    id: automatonCanvas

    //Domyślna wielkość płótna
    canvasSize.width: 400
    canvasSize.height: 400
    width: 400
    height: 400

    /*
        Renderowanie odbywa się w odzielnym wątku, co sprawia, że GUI można używać w sposób
        nieprzerwany (również w czasie rysowania płótna, GUI reaguje na polecenia użytkownika).
    */
    renderStrategy: Canvas.Threaded

    /*!
      \fn AutomatonCanvas.displayCellValues(mouseX : int, mouseY : int) : void

      Funkcja wywoływana po kliknięciu myszką na płótno. Znajduje ona właściwą komórkę
      na podstawie współrzędnych kursora myszy i wyświetla wartości parametrów komórki w GUI.
    */
    function displayCellValues(mouseX, mouseY) {
        //Zatrzymanie automatu, by umożliwić edycję wartości
        automatonContainer.stop()

        //Odnalezienie właściwej komórki na podstawie współrzędnych kursora myszy
        var cell = automatonContainer.getAutomatonGraphics().getCellFromMouse(mouseX, mouseY);

        //Pobieranie wartości parametrów z wybranej komórki
        var
            r = automatonContainer.getAutomaton().getCellValue(cell, "r"),
            g = automatonContainer.getAutomaton().getCellValue(cell, "g"),
            b = automatonContainer.getAutomaton().getCellValue(cell, "b"),
            border = automatonContainer.getAutomaton().getCellValue(cell, "border"),
            character = automatonContainer.getAutomaton().getCellValue(cell, "char"),
            state = automatonContainer.getAutomaton().getCellValue(cell, "state"),
            add = automatonContainer.getAutomaton().getCellValue(cell, "add")
        ;

        //Zapamiętanie, którą komórkę zaznaczyliśmy
        automatonContainer.getAutomatonGraphics().setSelectedCell(cell);

        var values = [r, g, b, border, character, state];

        //Usunięcie poprzednich wierszy wartości
        for (var i = 0; i < cellValues.valuesColumn.children.length; ++i)
            cellValues.valuesColumn.children[i].destroy();

        //Zaznaczanie radioboxów domyślnej wartości parametrów
        cellValues.nullValue.selectAllRadios();

        //Wyświetlanie wartości parametrów w GUI
        for (var i = 0; i < values.length; ++i)
            if (values[i] !== null) {
                var v = Qt.createComponent("CellValue.qml");
                var sprite = v.createObject(cellValues.valuesColumn, {"value" : values[i]});

                sprite.valueRepeater.itemAt(i).checked = true;
            }
    }

    //Eventy
    MouseArea {
        anchors.fill: parent
        onClicked: {
            displayCellValues(mouseX, mouseY)
        }
    }
}
