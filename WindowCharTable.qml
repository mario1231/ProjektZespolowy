import QtQuick 2.8
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "Files.js" as FilesScript

/*!
  \qmltype WindowCharTable

  Okno służące do wyboru znaku i wpisaniu go jako parametr zaznaczonej komórki.
  */

Rectangle {
    property alias charTable: charTable

    property var chars : []
    property var values : []
    property var curChar : 0
    property var textBoxToUpdate : null

    id: charTable
    color: "#88000000"

    //visible: false
    anchors.fill: parent

    /*!
      \fn WindowCharTable.updateChars(charsAndValues : Array(String[], float[]) : void

      Funkcja zapisuje do tablicy wczytaną z pliku tablicę znaków \a charsAndVales wraz
      z ich wartościami.
      */
    function updateChars(charsAndVales) {
        chars = [];
        values = [];

        for (var i = 0; i < charsAndVales.length; ++i) {
            chars.push(charsAndVales[i][0]);
            values.push(charsAndVales[i][1]);
        }

        repeater.model = chars;
    }

    // -> OKNO
    Rectangle {
        width: 600
        height: 400
        color: "#eee"
        anchors.centerIn: parent

        // -> OKNO - TYTUŁ
        Text {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.leftMargin: 10
            width: 200
            height: 20
            verticalAlignment: Text.AlignVCenter
            text: "Wybierz znak"
        }
        // <- OKNO - TYTUŁ


        // -> OKNO - PRZYCISK ZAMYKANIA
        Image {
            id: closeButton
            width: 20
            height: 20
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 10
            source: "img/close.png"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: charTable.visible = false;
            }
        }
        // <- OKNO - PRZYCISK ZAMYKANIA


        // -> OKNO - TABELA ZNAKÓW
        Grid {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: closeButton.bottom
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 40

            spacing: 20
            columns: 8

            // -> OKNO - TABELA ZNAKÓW - ZNAKI
            Repeater {
                id: repeater
                model: chars

                Rectangle {
                    color: "#aaa"
                    width: 50
                    height: 50
                    Text {
                        id: textBox
                        anchors.centerIn: parent
                        text: modelData
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {   
                            curChar = chars.indexOf(textBox.text.trim())

                            if (automatonContainer.getAutomatonGraphics().getSelectedCell() !== null) {
                                automatonContainer.getAutomaton().setCellValue(
                                    automatonContainer.getAutomatonGraphics().getSelectedCell(),
                                    "char", values[curChar]
                                );
                                automatonContainer.getAutomatonGraphics().draw();
                            }

                            textBoxToUpdate.text = values[curChar].toString()

                            charTable.visible = false
                        }
                    }
                }
            }
            // <- OKNO - TABELA ZNAKÓW - ZNAKI
        }
        // <- OKNO - TABELA ZNAKÓW
    }
    // <- OKNO
}
