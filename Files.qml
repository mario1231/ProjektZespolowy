import QtQuick 2.8
import QtQuick.Dialogs 1.2

import "Files.js" as FilesScript

/*!
    \qmltype Files

    \brief Klasa odpowiedzialna za wczytywanie i zapis do plików oraz komunikację z GUI w tym zakresie.

    Klasa odpowiedzialna za zapis i odczyt do plików. Stanowi komunikację między funkcjami
    wczytującymi i zapisującymi, a interfejsem użytkownika.
    Zawiera okna dialogowe do wczytywania i zapisu do pliku tekstowego iteracji i stanu automatu
    oraz okno dialogowe wczytywania pliku JavaScript.
  */
Item {
    property alias openFileDialog: openFileDialog
    property alias saveFileDialog: saveFileDialog
    property alias loadScriptDialog: loadScriptDialog
    property alias openIterationsFileDialog: openIterationsFileDialog
    property alias saveIterationsFileDialog: saveIterationsFileDialog

    /*!
      \qmlproperty Files.openFileDialog

      Okno dialogowe służące do odczytu pliku tekstowego.
      */
    FileDialog {
        id: openFileDialog
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        onAccepted: {
            FilesScript.deserialize(FilesScript.readFile(fileUrl))
        }
    }

    /*!
      \qmlproperty Files.loadScriptDialog

      \brief Okno dialogowe służące do odczytu pliku JavaScript.

      Po poprawnym załadowaniu pliku, tworzy nowy obiekt QML, który
      staje się reprezentacją nowego skryptu. Pobiera referencje do funkcji sterujących
      automatem ze stworzonego obiektu i zastępuje nimi domyślne funkcje sterujące
      zdefiniowane w pliku AutomatonScript.js
      */
    FileDialog {
        id: loadScriptDialog
        nameFilters: ["JavaScript (*.js;*.htc)", "All files (*)"]
        onAccepted: FilesScript.runLoadedScript(fileUrl);
    }

    /*!
      \qmlproperty Files.saveFileDialog

      Okno dialogowe służące do zapisu tekstu do pliku tekstowego.
      */
    FileDialog {
        id: saveFileDialog
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        selectExisting: false
        onAccepted: {
            FilesScript.saveFile(fileUrl, JSON.stringify(automatonContainer.getCells()))
        }
    }

    /*!
      \qmlproperty Files.openIterationsFileDialog

      Okno dialogowe służące do odczytu iteracji z pliku tekstowego.
      */
    FileDialog {
        id: openIterationsFileDialog
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        onAccepted: {
            FilesScript.deserializeIterations((FilesScript.readFile(fileUrl)))
        }
    }

    /*!
      \qmlproperty Files.saveIterationsFileDialog

      Okno dialogowe służące do zapisu iteracji do pliku tekstowego.
      */
    FileDialog {
        id: saveIterationsFileDialog
        nameFilters: ["Text files (*.txt)", "All files (*)"]
        selectExisting: false
        onAccepted: {
            FilesScript.saveFile(fileUrl, JSON.stringify(automatonContainer.getIterations()))
        }
    }
}
