import QtQuick 2.8
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "Automaton.js" as AutomatonScript
import "AutomatonGraphics.js" as AutomatonGraphicsScript
import "AutomatonLogics.js" as AutomatonLogicsScript
import "AutomatonScript.js" as AutomatonScriptScript
import "AutomatonIterator.js" as AutomatonIteratorScript

import "Files.js" as FilesScript

/*!
  \qmltype AutomatonContainer

  \brief Kontener w którym umiesczone jest płótno, służące do rysowania komórek automatu.

  Kontener w którym umiesczone jest płótno, służące do rysowania komórek automatu.
  Ten obiekt zawiera funkcje zwracające referencję do wszystkich obiektów automatu,
  funkcje zatrzymujące i uruchamiające działanie automatu, pobierające oraz zapisujące
  stan automatu oraz tablicę iteracji.
  */
Rectangle {
    property alias automatonContainer: automatonContainer
    property alias timer: timer
    property alias iteratorTimer: iteratorTimer
    property alias automatonContainerScrollView: automatonContainerScrollView
    property alias automtonScrollContainer: automtonScrollContainer

    /*!
      \fn AutomatonContainer.init() : void

      Funkcja tworząca wszystkie obiekty automatu oraz uruchamiająca funkcję inicjalizującą
      obiekty automatu i komórki.
      */
    function init() {
        //Tworzenie obiektów automatu
        AutomatonScript.createAutomaton();
        AutomatonLogicsScript.createAutomatonLogics(AutomatonScript.automaton);
        AutomatonGraphicsScript.createAutomatonGraphics(AutomatonScript.automaton);
        AutomatonScriptScript.createAutomatonScript(AutomatonLogicsScript.automatonLogics);
        AutomatonIteratorScript.createAutomatonIterator(AutomatonScript.automaton);

        //Wczytanie domyślnego skryptu
        FilesScript.runLoadedScript("scripts/Game_Of_Life.js");

        //Inicjalizacja i rysowanie automatu
        start();
    }

    /*!
      \fn AutomatonContainer.start() : void

      Funkcja inicjalizująca parametry automatu oraz komórek oraz rysująca
      automat w początkowym stanie. Funkcja ta resetuje iteracje automatu.
      */
    function start() {
        AutomatonScript.automaton.init();
        AutomatonScript.automaton.calculate();
        AutomatonScriptScript.automatonScript.init();
        AutomatonGraphicsScript.automatonGraphics.draw();
        AutomatonIteratorScript.automatonIterator.reset();
    }

    /*!
      \fn AutomatonContainer.update() : void

      Funkcja aktualizująca stan automatu. Oblicza nowe wartości komórek,
      rysuje zaktuwalizowany automat oraz dodaje iterację do tablicy iteracji - jeśli to konieczne.
      */
    function update() {
        //Aktualizacja wartości automatu
        AutomatonScript.automaton.calculate();
        AutomatonIteratorScript.automatonIterator.addIteration();
        AutomatonScriptScript.automatonScript.update();
        AutomatonGraphicsScript.automatonGraphics.draw();
    }

    /*!
      \fn AutomatonContainer.stop() : void

      Funkcja zatrzymująca działanie automatu.
      */
    function stop() {
        timer.stop();
    }

    /*!
      \fn AutomatonContainer.getAutomaton() : Automaton

      Funkcja zwracająca referencję do instancji głównego obiektu automatu.
      */
    function getAutomaton() { return AutomatonScript.automaton; }

    /*!
      \fn AutomatonContainer.getAutomatonGraphics() : AutomatonGraphics

      Funkcja zwracająca referencję do instancji obiektu odpowiedzialnego za grafikę automatu.
      */
    function getAutomatonGraphics() { return AutomatonGraphicsScript.automatonGraphics; }

    /*!
      \fn AutomatonContainer.getAutomatonLogics() : AutomatonLogics

      Funkcja zwracająca referencję do instancji obiektu odpowiedzialnego za logikę automatu.
      */
    function getAutomatonLogics() { return AutomatonLogicsScript.automatonLogics; }

    /*!
      \fn AutomatonContainer.getAutomatonScript() : AutomatonScript

      Funkcja zwracająca referencję do instancji obiektu odpowiedzialnego za skrypt automatu.
      */
    function getAutomatonScript() { return AutomatonScriptScript.automatonScript; }

    /*!
      \fn AutomatonContainer.getAutomatonIterator() : AutomatonIterator

      Funkcja zwracająca referencję do instancji obiektu odpowiedzialnego za iterator automatu.
      */
    function getAutomatonIterator() {return AutomatonIteratorScript.automatonIterator; }

    /*!
      \fn AutomatonContainer.setCells(object : Cell[][]) : void

      Funkcja kopiująca tablicę komórek \a object i zapisująca ją w miejscu aktualnej
      tablicy komórek.
      Po zaktualizowaniu wartości komórek automat jest rysowany z nowymi wartościami.
      Funkcja aktualizuje również niezbędne elementy GUI - wyświetlające wielkość automatu.
      */
    function setCells(object) {
        if (typeof object === "undefined")
            return;

        AutomatonScript.automaton.n = object.length;
        AutomatonScript.automaton.calculate();
        automatonControls.automatonSizeSlider.value = object.length;
        automatonControls.automatonSizeText.text = object.length.toString();

        AutomatonScript.automaton.cells = object;
        AutomatonGraphicsScript.automatonGraphics.draw();
    }

    /*!
      \fn AutomatonContainer.setIterations(object : Cell[][][]) : void

      Funkcja kopiująca tablicę iteracji \a object i zapisująca ją w miejscu aktualnej
      tablicy iteracji.
      Po zaktualizowaniu tablicy iteracji automat jest rysowany z nowymi wartościami (w pierwszej zapisanej iteracji)
      Funkcja aktualizuje również niezbędne elementy GUI - wyświetlające wielkość automatu.
      */
    function setIterations(object) {
        if (typeof object === "undefined")
            return;

        AutomatonScript.automaton.n = object[0].length;
        AutomatonScript.automaton.calculate();
        automatonControls.automatonSizeSlider.value = object[0].length;
        automatonControls.automatonSizeText.text = object[0].length.toString();

        AutomatonIteratorScript.automatonIterator.iterations = object;
        AutomatonIteratorScript.automatonIterator.firstIteration();
    }

    /*!
      \fn AutomatonContainer.getCells() : Cell[][]

      Funkcja zwracająca referencję do tablicy komórek.
      */
    function getCells() {
        return AutomatonScript.automaton.cells;
    }

    /*!
      \fn AutomatonContainer.getIterations() : Cell[][][]

      Funkcja zwracająca referencję do tablicy iteracji.
      */
    function getIterations() {
        return AutomatonIteratorScript.automatonIterator.iterations;
    }

    id: automatonContainer
    color: "transparent"

    width: 200
    height: 200

    anchors.top: menu.bottom
    anchors.topMargin: 40
    anchors.bottomMargin: 40
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.leftMargin: 40
    anchors.right: automatonControls.right
    anchors.rightMargin: 40

    // -> TIMER STERUJĄCY AUTOMATEM
    Timer {
        id: timer
        onTriggered: parent.update()
        repeat: true
        interval: 100
    }
    // <- TIMER STERUJĄCY AUTOMATEM


    // -> TIMER STERUJĄCY ITERATOREM
    Timer {
        id: iteratorTimer
        onTriggered: AutomatonIteratorScript.automatonIterator.iterate()
        repeat: true
        interval: 100
    }
    // <- TTIMER STERUJĄCY ITERATOREM


    // -> kONTENER SCROLLOWALNY W KTÓRYM UMIESZCZONE JEST PŁÓTNO AUTOMATU
    Rectangle {
        id: automtonScrollContainer
        width: parent.width > parent.height ? parent.height : parent.width
        height: width
        anchors.centerIn: parent
        color: "transparent"

        ScrollView {
            id: automatonContainerScrollView
            anchors.centerIn: parent
            clip: true
            width: 200
            height: 200

            Row {
                AutomatonCanvas {
                    id: automatonCanvas
                    width: 100
                    height: 100
               }
           }
       }
    }
   // <- kONTENER SCROLLOWALNY W KTÓRYM UMIESZCZONE JEST PŁÓTNO AUTOMATU
}
