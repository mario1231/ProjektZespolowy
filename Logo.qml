import QtQuick 2.8
/*!
  \qmltype Logo

  Klasa ta reprezentuje ikonę aplikacji.
  */
Image {
    id: logo
    width: 60
    height: 60
    anchors.top: parent.top
    anchors.topMargin: 20
    anchors.right: parent.right
    anchors.rightMargin: 20
    source: "img/icon.png"
    fillMode: Image.PreserveAspectCrop
}
