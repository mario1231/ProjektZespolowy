//Instancja obiektu odpowiedzialnego za grafikę automatu.
var automatonGraphics;

/*!
  \fn AutomatonGraphics.createAutomatonGraphics(automaton : Automaton) : void

  Funkcja tworząca obiekt odpowiedzialny za grafikę automatu.
  */
function createAutomatonGraphics(automaton) {
    automatonGraphics = new AutomatonGraphics(automaton);
}

/*!
  \qmltype AutomatonGraphics

  \brief Klasa odpowiedzialna za rysowanie automatu komórkowego.

    Klasa która reprzentuje graficzny interfejs \a automatu komórkowego
    W konstruktorze pobiera parametr \a automaton, który jest
    referencją do głównego obiektu automatu
  */
function AutomatonGraphics(automaton) {
    this.automaton = automaton;
    this.canvas = automaton.canvas;
    this.selectedCell = null;
}


/*!
  \fn AutomatonGraphics.hsvToRgb(hsv: int[3]) : int[3]

  Funkcja konwertująca kolor \a hsv do na rgb
  */
AutomatonGraphics.prototype.hsvToRgb = function(hsv)
{
    var r=0, g=0, b=0;
    var i,p,f,q,t;

    if (hsv[2] === 0)
    {
        r = g = b = 0;
    }
    else
    {
        hsv[0] /= 60;

        i = Math.floor((hsv)[0]);
        f = (hsv)[0]-i;
        p = (hsv)[2]*(1-(hsv)[1]);
        q = (hsv)[2]*(1-((hsv)[1]*f));
        t = (hsv)[2]*(1-((hsv)[1]*(1-f)));

        if (i===0)
        {
            r=(hsv)[2];
            g=t;
            b=p;
        }
        else if (i === 1)
        {
            r=q;
            g=(hsv)[2];
            b=p;
        }
        else if (i === 2)
        {
            r=p;
            g=(hsv)[2];
            b=t;
        }
        else if (i === 3)
        {
            r=p;
            g=q;
            b=(hsv)[2];
        }
        else if (i === 4)
        {
            r=t;
            g=p;
            b=(hsv)[2];
        }
        else if (i === 5)
        {
            r=(hsv)[2];
            g=p;
            b=q;
        }
    }

        (hsv)[0] = Math.floor(r * 255);
        (hsv)[1] = Math.floor(g * 255);
        (hsv)[2] = Math.floor(b * 255);

        return hsv;
}

/*!
  \fn AutomatonGraphics.rgbToHsv(rgb : int[3]) : int[3]

  Funkcja konwertująca kolor \a rgb do na hsv
  */
AutomatonGraphics.prototype.rgbToHsv = function(rgb)
{
        var r = (rgb)[0] / 255;
        var g = (rgb)[1] / 255;
        var b = (rgb)[2] / 255;

        var h = 0, s = 0, v = 0;

        var minRGB = Math.min(r, Math.min(g, b));
        var maxRGB = Math.max(r, Math.max(g, b));

        if (minRGB == maxRGB)
        {
            v = minRGB;
            (rgb)[0] = 0.0;
            (rgb)[1] = 0.0;
            (rgb)[2] = v.toPrecision(3);

            return rgb;
         }

         var d = (r == minRGB) ? g - b : ((b == minRGB) ? r - g : b - r);
         var h1 = (r == minRGB) ? 3 : ((b == minRGB) ? 1 : 5);

         h = 60 * (h1 - d / (maxRGB - minRGB));
         s = (maxRGB - minRGB) / maxRGB;
         v = maxRGB;

         (rgb)[0] = h.toPrecision(3);
         (rgb)[1] = s.toPrecision(3);
         (rgb)[2] = v.toPrecision(3);

         return rgb;
    }

/*!
  \fn AutomatonGraphics.toRGBString(rgb : int[3]) : String

  Funkcja konwertująca 3 elementową tablicę RGB do formatu "rgb(R,G,B)"
  */
AutomatonGraphics.prototype.toRGBString = function(rgb) {
    return "rgb(" + (rgb)[0] + ", " + (rgb)[1] + ", " + (rgb)[2] + ")";
}

/*!
  \fn AutomatonGraphics.setSelectedCell(cell : Cell) : void

  Funkcja zapisuje w graficznym obiekcie automatu informację o tym,
  która komórka została kliknięta.
  */
AutomatonGraphics.prototype.setSelectedCell = function(cell) {
    this.selectedCell = cell;
}

/*!
  \fn AutomatonGraphics.setSelectedCell(void) : void

  Funkcja zapisuje w graficznym obiekcie automatu informację o tym,
  która komórka została kliknięta.
  */
AutomatonGraphics.prototype.getSelectedCell = function() {
    return this.selectedCell;
}
/*!
  \fn AutomatonGraphics.getCellFromMouse(x : int, y : int) : Cell

  Fukcja na podstawie współrzędnych \a x i \a y myszy zwraca komórkę
  na którą kliknął użytkownik.
  */
AutomatonGraphics.prototype.getCellFromMouse = function(x, y) {
    var i = Math.floor(x / this.automaton.rectSize);
    var j = Math.floor(y / this.automaton.rectSize);

    return this.automaton.cells[i][j];
}

/*!
  \fn AutomatonGraphics.draw(void) : void

    Funkcja rysująca komórki w płótnie automatu.
    Komórka graficznie składać się może z tła, obramowania, oraz znaku wpisanego w środek komórki.
  */
AutomatonGraphics.prototype.draw = function() {
    // czyszczenie obiektu canvas
    this.automaton.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // rysowanie komórek
    for (var i = 0; i < this.automaton.n; ++i)
        for (var j = 0; j < this.automaton.n; ++j) {
            // obramowanie komórki
            var border;
            border = this.automaton.getCellValue(this.automaton.cells[i][j], "border") !== null ?
                this.automaton.getCellValue(this.automaton.cells[i][j], "border") :
                parseFloat(cellValues.nullValue.valueTextField.text);

            if (border > 0.0) {
                this.automaton.ctx.fillStyle = this.toRGBString([255, 0, 255]);

                this.automaton.ctx.fillRect(i * this.automaton.rectSize,
                    j * this.automaton.rectSize,
                    this.automaton.rectSize,
                    this.automaton.rectSize
                );
            }

            // tło komórki
            var realRectSize = this.automaton.rectSize - 2 * border;   
            var colorR = this.automaton.getCellValue(this.automaton.cells[i][j], "r") !== null ?
                this.automaton.getCellValue(this.automaton.cells[i][j], "r") :
                parseFloat(cellValues.nullValue.valueTextField.text);
            var colorG = this.automaton.getCellValue(this.automaton.cells[i][j], "g") !== null ?
                this.automaton.getCellValue(this.automaton.cells[i][j], "g") :
                parseFloat(cellValues.nullValue.valueTextField.text);
            var colorB = this.automaton.getCellValue(this.automaton.cells[i][j], "b") !== null ?
                this.automaton.getCellValue(this.automaton.cells[i][j], "b") :
                parseFloat(cellValues.nullValue.valueTextField.text);

            var color = [colorR, colorG, colorB];

            this.automaton.ctx.fillStyle = this.toRGBString(color);
            this.automaton.ctx.fillRect(
                i * this.automaton.rectSize + border,
                j * this.automaton.rectSize + border,
                realRectSize,
                realRectSize
            );


            var cVal;
            cVal = this.automaton.getCellValue(this.automaton.cells[i][j], "char") !== null ?
                this.automaton.getCellValue(this.automaton.cells[i][j], "char") :
                parseFloat(cellValues.nullValue.valueTextField.text);

            // znak w komórce
            var fontSize = realRectSize > 50 ? 30 : realRectSize * 2/3;
            var c = " ";

            if (charTable.chars.length > 0) {
                for (var k = 0; k < charTable.values.length; ++k)
                    if (charTable.values[k] <= cVal)
                        c = charTable.chars[k];
            }

            // konwertujemy na HSV i bierzemy ostatnią wartość
            var charColor = this.rgbToHsv(color)[2] > 0.5 ? "#000" : "#fff";

            this.automaton.ctx.fillStyle = charColor;
            this.automaton.ctx.font = fontSize + "px Arial";
            this.automaton.ctx.textAlign = "center";

            this.automaton.ctx.fillText(c,
                         i * (this.automaton.rectSize) + (this.automaton.rectSize) / 2,
                         j * (this.automaton.rectSize) + (this.automaton.rectSize) / 2 + fontSize / 2
            );

    }

    this.automaton.canvas.requestPaint();
}
