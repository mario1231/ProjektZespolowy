import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppRadioButton

  Klasa reprezentująca RadioButton o zmodyfikowanym wyglądzie.
  */
RadioButton {
    id: control
    text: "testowy "

    indicator: Rectangle {
        implicitWidth: 20
        implicitHeight: 20
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        border.color: control.down ? "#aaa" : "#666"

        Rectangle {
            width: 10
            height: 10
            x: 5
            y: 5
            color: control.down ? "#1ba08e" : "#0e7c6d"
            visible: control.checked
        }
    }

    contentItem: Text {
        text: control.text
        opacity: enabled ? 1.0 : 0.4
        color: control.down ? "#fff" : "#ddd"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        leftPadding: control.indicator.width + control.spacing
    }
}
