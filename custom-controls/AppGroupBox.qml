import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppGroupBox

  Klasa reprezentująca GroupBox o zmodyfikowanym wyglądzie.
  */
GroupBox {
    id: control
    padding: 5

    background: Rectangle {
            y: control.topPadding - control.padding
            width: parent.width
            height: parent.height - control.topPadding + control.padding
            color: "transparent"
            border.color: "#66ffffff"
            border.width: 1
        }

   label: Label {
            x: 10
            width: control.availableWidth
            text: control.title
            color: "#21be2b"
            elide: Text.ElideRight
        }
}
