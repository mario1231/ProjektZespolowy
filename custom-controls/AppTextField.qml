import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppTextField

  Klasa reprezentująca TextField o zmodyfikowanym wyglądzie.
  */
TextField {
    id: control
    selectByMouse: true

    background: Rectangle {
        implicitWidth: 50
        implicitHeight: 30
        color: control.enabled ? "#eee" : "#ddd"
        border.color: control.enabled ? "#1ba08e" : "#0e7c6d"
    }
}
