import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppSpinBox

  Klasa reprezentująca SpinBox o zmodyfikowanym wyglądzie.
  */
SpinBox {
    id: control
    editable: false

    contentItem: TextInput {
        z: 2
        text: control.textFromValue(control.value, control.locale)

        font: control.font
        color: "#333"
        selectionColor: "#0e7c6d"
        selectedTextColor: "#fff"
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
    }

    up.indicator: Rectangle {
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        implicitWidth: 32
        implicitHeight: 32
        color: control.up.pressed ? "#ddd" : "#eee"
        border.color: enabled ? "#333" : "#666"

        Text {
            text: "+"
            font.pixelSize: control.font.pixelSize * 2
            color: "#0e7c6d"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    down.indicator: Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        implicitWidth: 32
        implicitHeight: 32
        color: control.down.pressed ? "#ddd" : "#eee"
        border.color: enabled ? "#333" : "#666"

        Text {
            text: "-"
            font.pixelSize: control.font.pixelSize * 2
            color: "#0e7c6d"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    background: Rectangle {
        implicitWidth: 140
        border.color: "#333"
    }
}
