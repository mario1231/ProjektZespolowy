import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppSlider

  Klasa reprezentująca Slider o zmodyfikowanym wyglądzie.
  */
Slider {
    id: control

    background: Rectangle {
            x: control.leftPadding
            y: control.topPadding + control.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: control.availableWidth
            height: implicitHeight
            color: "#ddd"

            Rectangle {
                width: control.visualPosition * parent.width
                height: parent.height
                color: "#1ba08e"
            }
        }

        handle: Rectangle {
            x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
            y: control.topPadding + control.availableHeight / 2 - height / 2
            implicitWidth: 10
            implicitHeight: 20
            color: "#0e7c6d"
            border.width: 0
        }
}
