import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppButton

  Klasa reprezentująca Button o zmodyfikowanym wyglądzie.
  */
Button {
    id: control
    text: "Przykładowy tekst"

    contentItem: Text {
        text: control.text
        opacity: enabled ? 1.0 : 0.3
        color: control.down ? "#000" : "#222"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideNone
        font.pixelSize: 12
    }

    background : Rectangle {
        implicitWidth: 100
        implicitHeight: 32
        opacity: enabled ? 1 : 0.4
        color: control.down ? "#ddd" : "#eee"
        border.color: control.down ? "#0e7c6d" : "#ddd"
        border.width: 1
    }
}
