import QtQuick 2.8
import QtQuick.Controls 2.2

/*!
  \qmltype AppSwitch

  Klasa reprezentująca Switch o zmodyfikowanym wyglądzie.
  */
Switch {
    id: control

    indicator: Rectangle {
            implicitWidth: 40
            implicitHeight: 20
            x: control.leftPadding
            y: parent.height / 2 - height / 2
            color: control.checked ? "#1ba08e" : "#ddd"

            Rectangle {
                x: control.checked ? parent.width - width : 0
                y: -5
                width: 10
                height: 30
                color: control.down ? "#eee" : "#fff"
            }
        }

        contentItem: Text {
            text: control.text
            font: control.font
            opacity: enabled ? 1.0 : 0.3
            color: "#fff"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            leftPadding: control.indicator.width + control.spacing
        }
}
