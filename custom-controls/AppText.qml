import QtQuick 2.8

/*!
  \qmltype AppText

  Klasa reprezentująca Text o zmodyfikowanym wyglądzie.
  */
Text {
    height: 30
    verticalAlignment: Text.AlignVCenter
    color: "#fff"
}
