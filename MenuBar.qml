import QtQuick 2.8

import "Files.js" as FilesScript

/*!
  \qmltype MenuBar

  Klasa ta reprezentuje główne menu aplikacji. Menu zawiera przyciski oraz
  ozdobny slider.
  */
Rectangle {
    color: "#1c2d35"
    border.width: 0
    anchors.rightMargin: 0
    anchors.leftMargin: 0
    anchors.topMargin: 0
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.right: parent.right
    height: 50

    // -> SLIDER
    MenuSlider { id: menuSlider }
    // <- SLIDER

    // -> PRZYCISKI
    Row {
        id: rowMenu
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        spacing: 0

        MenuButton {
            id: openFileButton
            text: qsTr("Wczytaj stan automatu")

            MouseArea {
                id: openFileMouseArea
                anchors.fill: parent
                onClicked: {
                    files.openFileDialog.open()
                    automatonContainer.stop()
                }
            }
        }

        MenuButton {
            id: saveFileButton
            text: qsTr("Zapisz stan automatu")

            MouseArea {
                id: saveFileMouseArea
                anchors.fill: parent
                onClicked: {
                    files.saveFileDialog.open()
                    automatonContainer.stop()
                }
            }
        }

        MenuButton {
            id: loadIterationsButton
            text: qsTr("Wczytaj iteracje")

            MouseArea {
                id: loadIterationsMouseArea
                anchors.fill: parent
                onClicked: {
                    files.openIterationsFileDialog.open()
                    automatonContainer.stop()
                }
            }
        }

        MenuButton {
            id: saveIterationsButton
            text: qsTr("Zapisz iteracje")

            MouseArea {
                id: saveIterationsMouseArea
                anchors.fill: parent
                onClicked: {
                    files.saveIterationsFileDialog.open()
                    automatonContainer.stop()
                }
            }
        }

        MenuButton {
            id: loadScriptButton
            text: qsTr("Wczytaj skrypt")

            MouseArea {
                id: loadScriptMouseArea
                anchors.fill: parent
                onClicked: {
                    files.loadScriptDialog.open()
                    automatonContainer.stop()
                }
            }
        }

        MenuButton {
            id: editScriptButton
            text: qsTr("Edytuj skrypt")

            MouseArea {
                id: editScriptMouseArea
                anchors.fill: parent
                onClicked: {
                    scriptEditor.editingArea.text = FilesScript.readFile(automatonContainer.getAutomatonScript().url);
                    scriptEditor.visible = true;
                }
            }
        }

        MenuButton {
            id: authorsButton
            text: qsTr("Autorzy")

            MouseArea{
                id: authorsMouseArea
                anchors.fill: parent

                onClicked: {
                    authors.visible = true;
                }
            }
        }
    }
    // <- PRZYCISKI
}
