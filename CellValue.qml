import QtQuick 2.8
import QtQuick.Controls 2.2

import "custom-controls/"

/*
  \qmltype CellValue

  Reprezentuje wartość komórki w GUI. Jest wierszem tabeli opisanej w pliku CellValues.qml.
  Zawiera radioboxy umożliwiające wybór parametru oraz pole tekstowe wyświetlające wartość
  wybranego parametru oraz umożliwjające jego zmianę.
  */
Rectangle {
    id: cellValue
    anchors.left: parent.left
    anchors.right: parent.right
    height: 30
    color: "#00000000"

    /*!
      \qmlproperty CellValue.groups

      Parametr przechowujący grupy radioboxów poszczególnych parametrów komórki.
      */
    property var groups : [
        cellValues.rGroup, cellValues.gGroup, cellValues.bGroup,
        cellValues.borderGroup, cellValues.charGroup, cellValues.stateGroup,
        cellValues.addGroup
    ]

    property var value : 0
    property alias valueTextField: valueTextField
    property alias valueRepeater: valueRepeater

    /*!
        \fn CellValue.updateValue(void) : void

        Procedura aktualizująca wartości wszystkich parametrów komórek
        oraz odświeżająca automat na ekranie.
        Jest ona wywoływana po zmianie wartości dowolnego parametru w GUI.
      */
    function updateValue(isNull) {
        var checked = [];
        var params = ["r", "g", "b", "border", "char", "state", "add"];

        //Sprawdzamy które parametry mają przypisaną wartość, która nie jest domyślna.
        for (var i = 0; i < valueRepeater.count; ++i)
            if (valueRepeater.itemAt(i).checked)
                checked.push(i);

        if (checked.indexOf(4) >= 0) {
            charButton.visible = true;
            charButton.width = 80;
            charButton.anchors.rightMargin = 5;
        }
        else {
            charButton.visible = false;
            charButton.width = 0;
            charButton.anchors.rightMargin = 0;
        }

        //Dla tych parametrów aktualizujemy wartości
        for (var i = 0; i < checked.length; ++i) {
            automatonContainer.getAutomaton().setCellValue(
                automatonContainer.getAutomatonGraphics().getSelectedCell(),
                params[checked[i]],
                !isNull ? parseFloat(valueTextField.text) : null
            );
        }

        //Aktualizujemy automat na ekranie
        automatonContainer.getAutomatonGraphics().draw();
    }

    /*!
      \fn CellValue.selectAllRadios(void) : void

      Funkcja zaznaczająca wszystkie radioboxy w wierszu.
      */
    function selectAllRadios() {
        for (var i = 0; i < valueRepeater.count; ++i)
            valueRepeater.itemAt(i).checked = true;
    }

    // -> RADIOBOXY UMOŻLIWIAJĄCE WYBÓR PARAMETRU
    Row {
        id: row
        spacing: 5

        Repeater {
            id: valueRepeater
            model: groups   //Grupy RadioBox'ów opisane w pliku CellValues.qml.

            RadioButton {
                id: radioButton
                width: 30
                height: 30
                padding: 0

                //Wybieranie odpowiedniej grupy przycisku z listy model.
                ButtonGroup.group: modelData

                //Zmiana wyglądu RadioBox'a
                indicator: Rectangle {
                    color: "#26373f"
                    implicitWidth: 30
                    implicitHeight: 30

                    Rectangle {
                        width: 10
                        height: 10
                        x: 10
                        y: 10
                        color: "#1ba08e"
                        visible: radioButton.checked
                    }
                }
            }

        }
    }
    // <- RADIOBOXY UMOŻLIWIAJĄCE WYBÓR PARAMETRU

    // -> POLE TEKSTOWE SŁUŻĄCE DO WYŚWIETLANIA ORAZ ZMIANY WARTOŚCI PARAMETRU
    AppTextField {
        id: valueTextField
        anchors.right: charButton.left
        anchors.left: row.right
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        horizontalAlignment: Text.AlignHCenter
        validator: DoubleValidator {}
        text: parent.value

        //Gdy wciśniemy enter aktualizujemy wartości komórki
        Keys.onReturnPressed: {
            parent.updateValue()
        }
    }
    // <- POLE TEKSTOWE SŁUŻĄCE DO WYŚWIETLANIA ORAZ ZMIANY WARTOŚCI PARAMETRU

    // -> PRZYCISK DO WCZYTYWANIA ZNAKU
    AppButton {
        id: charButton
        anchors.right: parent.right
        anchors.rightMargin: 0
        visible: false
        text: "Znak"
        width: 0
        height: 30
        onClicked: {
            charTable.textBoxToUpdate = valueTextField
            charTable.visible = true
        }
    }

    // <- PRZYCISK DO WCZYTYWANIA ZNAKU
}
