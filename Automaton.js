﻿//Instancja głównego obiektu automatu
var automaton;

/*!
    \qmltype Cell
    \brief Klasa reprezentująca pojedynczą komórkę automatu.

    Klasa reprezentująca pojedynczą komórkę automatu.
    Jej obiekty są tablicą 7-elementową przechowującą 6 zmiennych typu float
    reprezentujących określone parametry komórki oraz tablicę typu float
    zawierającą dodatkowe (opcjonalne) parametry komórki.
*/
var Cell = function() {
    return [
        null,           //R/H
        null,           //G/S
        null,           //B/V
        null,           //Wielkość obramowania
        null,           //Znak w komórce
        null,           //Stan komórki
        []              //Tablica float dodatkowych wartości
    ];
}

/*!
  \qmltype Automaton
  \brief Glowna klasa automatu.

  Glowna klasa automatu. Przechowuje ona wszystkie potrzebne wartości automatu,
  takie jak: liczbę kokórek w rzędzie, wielkość pojedynczej komórki,
  referencje do odpowiednich kontenerów oraz funkcje inicjalizujące automat,
  dopasowujace go do okna oraz kontenerow.
*/
function Automaton() {
    this.ctx = null;
    this.zoom = 1.0
    this.rectSize = 10;
    this.n = 5;
    this.canvas = automatonCanvas;

    this.isRGB = true;

    this.nullCell = new Cell();

    this.init();
    this.calculate();
}

/*!
  \fn Automaton.createAutomaton() : void

  Funkcja tworząca główny obiekt automatu komórkowego
*/
function createAutomaton() {
    automaton = new Automaton();
}

/*!
  \fn Automaton.init() : void
  \brief Funkcja inicjaluzująca komórki automatu.

  Funkcja tworzy tablicę komórek o odpowiedniej wielkości oraz
  tworzy instancje nowych komórek. Funkcja służy również do pobrania kontekstu
  z płótna.
*/
Automaton.prototype.init = function()
{
    this.ctx = this.canvas.getContext("2d");

    //Inicjalizacja tablicy komórek
    this.cells = [];

    for (var i = 0; i < this.n; ++i)
    {
        this.cells[i] = [];

        for (var j = 0; j < this.n; ++j) {
            this.cells[i][j] = new Cell();
        }
    }
}

/*!
  \fn Automaton.getCellValue(cell : Cell, key : String) : float | float[]

  \brief Funkcja zwracająca parametr \a key komórki \a cell.

  Funkcja zwraca parametr \a key komórki \a cell. Uwaga - funkcja może zwrócić zarówno
  liczbę typu float jak i tablicę liczb typu float, w zależności od zadanego klucza.
  Jeśli klucz istnieje i nie jest to klucz reprezentujący tablicę dodatkowych wartości -
  funkcja zwróci liczbę typu float, w przeciwnym wypadku zwróci tablicę float dodatkowych
  wartości komórki.
  */
Automaton.prototype.getCellValue = function(cell, key) {
    switch (key.toLowerCase()) {
        case "r": case "h": return cell[0];
        case "g": case "s": return cell[1];
        case "b": case "v": return cell[2];

        case "rgb": case "hsv": return [cell[0], cell[1], cell[2]];
        case "border": return cell[3];
        case "char": return cell[4];
        case "state": return cell[5];

        default: return cell[6];
    }
}

/*!
  \fn Automaton.setCellValue(cell : Cell, key : String, value : float | float[]) : void

  Funkcja aktualizująca wartość \a value parametru \a key w komórce \a cell.
  */
Automaton.prototype.setCellValue = function(cell, key, value) {
    switch (key.toLowerCase()) {
        case "r": case "g": case "b": this.isRGB = true; break;
        case "h": case "s": case "v": this.isRGB = false; break;
    }

    switch (key.toLowerCase()) {
        case "r": cell[0] = value; break;
        case "g": cell[1] = value; break;
        case "b": cell[2] = value; break;

        case "h": cell[0] = value; break;
        case "s": cell[1] = value; break;
        case "v": cell[2] = value; break;

        case "rgb":
        case "hsv":
            cell[0] = value[0];
            cell[1] = value[1];
            cell[2] = value[2];
            break;

        case "border": cell[3] = value; break;
        case "char": cell[4] = value; break;
        case "state": cell[5] = value; break;

        default: cell[6] = value; break;
    }
}

/*!
  \fn Automaton.setAllCells(cell : Cell) : void

  Funkcja pobiera wartości parametrów komórki \a cell i przypisuje je wszystkim
  komórkom automatu.
  */
Automaton.prototype.setAllCells = function(cell) {
    if (cell === null)
        return;

    for (var i = 0; i < this.cells.length; ++i)
        for (var j = 0; j < this.cells.length; ++j)
            for (var k = 0; k < cell.length; ++k)
                this.cells[i][j][k] = cell[k];
}

/*!
    \fn Automaton.calculate() : void

    Funkcja aktualizjąca wielkość automatu i komórek w zależności od wielkości kontenera i zoomu.
  */
Automaton.prototype.calculate = function() {
    //Ustaw wielkość komórki w zależności od wielkości kontenera
    this.rectSize = Math.floor(automtonScrollContainer.width / this.n);

    //Ustaw wielkość ScroollView
    automatonContainerScrollView.width = this.rectSize * this.n;
    automatonContainerScrollView.height = automatonContainerScrollView.width;

    this.rectSize *= this.zoom;

    //Ustaw wielkość canvas
    this.canvas.width =this.rectSize * this.n;
    this.canvas.height = this.rectSize * this.n;
    this.canvas.canvasSize.width = this.canvas.width;
    this.canvas.canvasSize.height = this.canvas.height;
}
