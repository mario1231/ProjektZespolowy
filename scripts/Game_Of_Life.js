function init()
{
    this.setNeighbourhood(8);

    for (var i = 0; i < this.getN(); ++i)
        for (var j = 0; j < this.getN(); ++j)
            this.set(this.getCell(i, j), "rgb", (Math.random() > 0.5 ? [0,0,0] : [255,255,255]));
}

/*
    Funkcja inicjalizuje komórki w strukturę o nazwie Glider, który jest jednym ze
    struktur ruchomych w Grze o życie Conway'a.
  */
function initGlider()
{
    for(var i = 0; i < this.getN(); i++)
    {
        for(var j = 0; j < this.getN(); j++)
        {
            if((j == 1 && i == 2) || (j == 2 && i == 3) || (j == 3 && i == 1) || (j == 3 && i == 2) || (j == 3 && i == 3))
            {
                this.set(this.getCell(i, j), "rgb", [255, 255, 255]);
            }
            else
            {
                this.set(this.getCell(i, j), "rgb", [0, 0, 0]);
            }
        }
    }
}

/*
    Funkcja na potrzeby Gry o życie.
    Sprawdza czy, dana w argumencie wywołnia, komórka jest żywa czy martwa.
  */
function isAlive(color)
{
    if (color[0] === 255 && color[1] === 255 && color[2] === 255)
    {
        return true;
    }
    else if(color[0] === 0 && color[1] === 0 && color[2] === 0)
    {
        return false;
    }
}

/*
    Funkcja odpowiedzialna za działanie logiki automatu. Precyzuje reguły, na których
    opiera się działanie automatu.
  */
function update()
{
    var next = [];
    for (var i = 0; i < this.getN(); ++i)
    {
        next[i] = [];
        for (var j = 0; j < this.getN(); ++j)
        {
            next[i][j] = [0, 0, 0];
        }
    }

    for (var i = 0; i < this.getN(); ++i)
    {
        for (var j = 0; j < this.getN(); ++j)
        {
            var cur = this.getCell(i, j);
            var neighbours = this.getNeighbours(i, j);
            var alive = 0;

            for (var k = 0; k < neighbours.length; ++k)
            {
                if (isAlive(this.get(neighbours[k], "rgb")))
                {
                    alive++;
                }
            }

            if(isAlive(this.get(cur, "rgb")) && alive === 2 || alive === 3)
            {
                next[i][j] = [255, 255, 255];
            }
            else
            {
                next[i][j] = [0, 0, 0];
            }
        }
    }

    for (var i = 0; i < this.getN(); ++i)
    {
        for (var j = 0; j < this.getN(); ++j)
        {
            this.set(this.getCell(i, j), "rgb", next[i][j]);
        }
    }
}

