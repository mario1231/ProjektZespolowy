var nStates = 16; //nStates - ilość stanów (najlepiej 16)
var step = Math.floor(360 / nStates); //step - odległość stanów od siebie
var cellStates = []; //cellStates - tablica stanów

/*
  Inicjalizuje komórki. Nadaje im początkowy kolor wybrany losowo z 16 możliwych.
  Kolory są zapisane w tablicy stanów.
  */
function init()
{
    this.setNeighbourhood(8);

    for(var i = 0, j = 0; i < 360 || j < nStates; i+= step, j++)
        cellStates[j] = i;

    for (var i = 0; i < this.getN(); i++)
    {
        for (var j = 0; j < this.getN(); j++)
        {
            var cur = this.getCell(i, j);

            this.set(cur, "state", Math.floor(Math.random() * nStates));

            var curState = this.get(cur, "state");
            var curColor = [cellStates[curState], 1.0, 1.0];
            this.set(cur, "rgb", this.hsvToRgb(curColor));
        }
    }
}

/*
  Implementacja jednej z odmian cyklicznego automatu komórkowego.
  Najlepiej działą z sąsiedztwem MOORE'a.
  */
function update()
{
    var next = [];
    for (var i = 0; i < this.getN(); ++i)
    {
        next[i] = [];
        for (var j = 0; j < this.getN(); ++j)
        {
            next[i][j] = [0, 0, 0];
        }
    }

    var Snext = [];
    for (var i = 0; i < this.getN(); ++i)
    {
        Snext[i] = [];
        for (var j = 0; j < this.getN(); ++j)
        {
            Snext[i][j] = 0;
        }
    }

    for(var i = 0; i < this.getN(); i++)
    {
        for(var j = 0; j < this.getN(); j++)
        {
            var n = 0;

            var cur = this.getCell(i, j);
            var neighbours = this.getNeighbours(i, j);
            var curState = this.get(cur, "state") % nStates;

            for(var k = 0; k < neighbours.length; k++)
            {
                var curN = neighbours[k];
                var curNState = this.get(curN, "state") % nStates;

                if((curState + 1) % nStates === curNState)
                    n++;
            }

            // 1 or 0
            if(n >= 1)
            {
                var s = (curState + 1) % nStates;
                var tmp = [cellStates[s], 1.0, 1.0];

                next[i][j] = this.hsvToRgb(tmp);
                Snext[i][j] = s;
            }
            else
            {
                next[i][j] = this.get(cur, "rgb");
                Snext[i][j] = curState;
            }
        }
    }

    for(var i = 0; i < this.getN(); i++)
    {
        for(var j = 0; j < this.getN(); j++)
        {
            var cur = this.getCell(i, j);
            this.set(cur, "rgb", next[i][j]);
            this.set(cur, "state", Snext[i][j]);
        }
    }
}

