/*

    Zmienne, które można używać w skrypcie:
    n : Int
    reprezentuje ilość komórek w rzędzie (n^2 = wszystkie komórki)

    cells : Cell[]
    tablica przechowująca wszystkie komórki automatu

    Funkcje, które można używać w skrypcie:

    AutomatonScript.get(komórka : Cell, parametr : String) : Float
    pobiera wartość parametru z podanej komórki

    AutomatonScript.set(komórka : Cell, parametr : String, wartość : Float | Float[])
    zapisuja nową wartość parametru w podanej komórce

    AutomatonScript.getNeighbours(komórka : Cell) : Cell[]
    pobiera obiekty typu Cell reprezentujące sąsiadów podanej komórki

  */

/*
    Funkcja inicjalizująca komórki. Umożliwia konkretyzację sposobu wypełniania komórek
    automatu przy uruchamianiu skryptu. Gdy pusta, automat korzysta z domyslnej funkcji
    losującej parametry komórki.
  */
function init() {
    for (var i = 0; i < this.getN(); ++i) {
        for (var j = 0; j < this.getN(); ++j) {
            this.set(this.getCell(i, j), "rgb", [
                    Math.floor(Math.random() * 256),
                    Math.floor(Math.random() * 256),
                    Math.floor(Math.random() * 256)
                ]);
        }
    }
}

/*
    Funkcja odpowiedzialna za działanie logiki automatu. Precyzuje reguły, na których
    opiera się działanie automatu.
  */
function update() {
    for (var i = 0; i < this.getN(); ++i) {
        for (var j = 0; j < this.getN(); ++j) {
            var cur = this.getCell(i, j);
            var neighbours = this.getNeighbours(i, j);
            var averageColor = [0,0,0];

            for (var k = 0; k < neighbours.length; ++k) {
                averageColor[0] += this.get(neighbours[k], "rgb")[0];
                averageColor[1] += this.get(neighbours[k], "rgb")[1];
                averageColor[2] += this.get(neighbours[k], "rgb")[2];
            }

            averageColor[0] = Math.floor(averageColor[0] / neighbours.length);
            averageColor[1] = Math.floor(averageColor[2] / neighbours.length);
            averageColor[2] = Math.floor(averageColor[2] / neighbours.length);

            this.set(cur, "rgb", averageColor);
        }
    }
}
