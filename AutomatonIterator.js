//Instancja obiektu odpowiedzialnego za iteracje automatu
var automatonIterator;

/*!
    \fn AutomatonIterator.createAutomatonIterator(automaton : Automaton) : void

    Funkcja tworząca obiekt odpowiedzialny za iteracje \a automatu
  */
function createAutomatonIterator(automaton) {
    automatonIterator = new AutomatonIterator(automaton);
}

/*!
  \qmltype AutomatonIterator

  \brief Klasa odpowiedzialna za zapisywanie i manimulowanie iteracji.

  Klasa AutomatonIterator służy do zapisywania oraz przechowywania iteracji \a automatu.
  Dostarcza również metody służące do odtwarzania iteracji oraz przechodzenia między
  iteracjami zapisanymi w pamięci.
  */
function AutomatonIterator(automaton) {
    this.automaton = automaton;

    this.isEnabled = false;
    this.iterations = [];
    this.curIteration = 0;
}

/*!
  \fn AutomatonIterator.iterate(void) : void

  Funkcja odtwarzająca iteracje zapisane w pamięci.
  */
AutomatonIterator.prototype.iterate = function() {
    if (this.curIteration > this.iterations.length - 1) {
        iteratorTimer.stop();
        return;
    }

    automatonContainer.setCells(this.iterations[this.curIteration]);
    automatonContainer.getAutomatonGraphics().draw();

    ++this.curIteration;
}

/*!
  \fn AutomatonIterator.enable(void) : void

  Funkcja włączająca rejestrowanie iteracji.
  */
AutomatonIterator.prototype.enable = function() {
    this.isEnabled = true;
}

/*!
  \fn AutomatonIterator.disable(void) : void

  Funkcja wyłączająca rejestrowanie iteracji.
  */
AutomatonIterator.prototype.disable = function() {
    this.isEnabled = false;
}

/*!
  \fn AutomatonIterator.reset(void) : void

  Funkcja usuwająca wszystkie zapisane iteracje.
  */
AutomatonIterator.prototype.reset = function() {
    this.curIteration = 0;
    this.iterations = [];
}

/*!
  \fn AutomatonIterator.addIteration(void) : void

  Funkcja dodająca iterację. Zapisuje aktualny stan automatu jako nową iterację.
  */
AutomatonIterator.prototype.addIteration = function() {
    if (this.isEnabled) {
        this.iterations.push(JSON.parse(JSON.stringify(this.automaton.cells)));
        ++this.curIteration;
    }
}

/*!
  \fn AutomatonIterator.firstIteration(void) : void

  Funkcja przechodzi do pierwszej iteracji i odświeża automat.
  */
AutomatonIterator.prototype.firstIteration = function() {
    this.curIteration = 0;
    automatonContainer.setCells(this.iterations[this.curIteration]);
    automatonContainer.getAutomatonGraphics().draw();
}

/*!
  \fn AutomatonIterator.lastIteration(void) : void

  Funkcja przechodzi do ostatniej iteracji i odświeża automat.
  */
AutomatonIterator.prototype.lastIteration = function() {
    this.curIteration = this.iterations.length - 1;
    automatonContainer.setCells(this.iterations[this.curIteration]);
    automatonContainer.getAutomatonGraphics().draw();
}

/*!
  \fn AutomatonIterator.prevIteration(void) : void

  Funkcja przechodzi do poprzedniej iteracji i odświeża automat.
  */
AutomatonIterator.prototype.prevIteration = function() {
    if (this.curIteration > 0) {
        --this.curIteration;
        automatonContainer.setCells(this.iterations[this.curIteration]);
        automatonContainer.getAutomatonGraphics().draw();
    }
}

/*!
  \fn AutomatonIterator.nextIteration(void) : void

  Funkcja przechodzi do następnej iteracji i odświeża automat.
  W przypadku braku iteracji - tworzy nową poprzez wykonanie jednego cyklu życia automatu.
  */
AutomatonIterator.prototype.nextIteration = function() {
    if (this.curIteration < this.iterations.length - 1) {
        ++this.curIteration;
        automatonContainer.setCells(this.iterations[this.curIteration]);
    }
    else {
        automatonContainer.getAutomatonScript().update();
    }

    automatonContainer.getAutomatonGraphics().draw();
}
