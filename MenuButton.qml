import QtQuick 2.8

/*!
  \qmltype MenuButton

  Klasa reprezentuje pojedynczy przycisk menu.
  */
Text {
    property color textColor : "#ccc"
    property color hoverTextColor: '#fff'

    font.pixelSize: 14
    leftPadding: 15
    rightPadding: 15
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    anchors.top: parent.top
    anchors.topMargin: 0
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 0
    color: textColor

    /*!
      MouseArea -  steruje wykonywaniem animacji slidera menu.
      */
    MouseArea {
        hoverEnabled: true
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onContainsMouseChanged: {
            if (containsMouse)
            {
                menuSlider.menuSliderAnimation.stop()
                menuSlider.menuSliderAnimation.slideFrom = menuSlider.x
                menuSlider.menuSliderAnimation.slideTo = parent.x + rowMenu.x
                menuSlider.menuSliderAnimation.start()

                menuSlider.menuSliderWidthAnimation.stop()
                menuSlider.menuSliderWidthAnimation.slideFrom = menuSlider.width
                menuSlider.menuSliderWidthAnimation.slideTo = width
                menuSlider.menuSliderWidthAnimation.start()

                parent.color = parent.hoverTextColor
            }
            else
            {
                menuSlider.menuSliderAnimation.stop()
                menuSlider.menuSliderAnimation.slideFrom = menuSlider.x
                menuSlider.menuSliderAnimation.slideTo = -100
                menuSlider.menuSliderAnimation.start()

                menuSlider.menuSliderWidthAnimation.stop()
                menuSlider.menuSliderWidthAnimation.slideFrom = width
                menuSlider.menuSliderWidthAnimation.slideTo = 0
                menuSlider.menuSliderWidthAnimation.start()

                parent.color = parent.textColor
            }
        }
    }
}
