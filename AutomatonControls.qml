import QtQuick 2.8
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "custom-controls/"

import "Automaton.js" as AutomatonScript

/*!
   \qmltype AutomatonControls

   \brief Część interfejsu odpowiedzialna za edycję parametrów automatu i komórwk.

   Część interfejsu zawieająca pola służące do sterowania działaniem automatu,
   zmiany jego parametrów oraz pola służące do edycji i wyświetlania wartości komórek.
  */
Rectangle {
    id: automatonControls

    property alias cellValues: cellValues
    property alias automatonSizeText: automatonSizeText
    property alias automatonSizeSlider: automatonSizeSlider

    color: "#00000000"

    width: 500
    anchors.right: parent.right
    anchors.rightMargin: 40
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 40
    anchors.top: menu.bottom
    anchors.topMargin: 40

    // -> WIERSZ WIELKOŚCI, ZOOM'U
    RowLayout {
        id: automatonSizeRowLayout

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        // -> STEROWANIE WIELKOŚCIĄ AUTOMATU
        AppGroupBox {
            id: automatonSizeGruopBox

            Layout.fillWidth: true

            label : Text {
                text: "Rozmiar automatu"
                color: "#fff"
            }

            RowLayout {
                height: 40
                anchors.fill: parent
                spacing: 10

                // -> STEROWANIE WIELKOŚCIĄ AUTOMATU - POLE TEKSTOWE
                AppTextField {
                    id: automatonSizeText
                    width: 50
                    Layout.preferredWidth: 50
                    height: 30
                    text: "5"

                    onTextChanged: {
                            if (parseInt(text) > 200)
                                text = "200"

                            if (parseInt(text) < 5)
                                text = "5"

                            automatonSizeSlider.value = parseInt(text)
                            automatonContainer.getAutomaton().n = automatonSizeSlider.value
                            automatonContainer.start()
                        }
                }
                // <- STEROWANIE WIELKOŚCIĄ AUTOMATU - POLE TEKSTOWE


                // -> STEROWANIE WIELKOŚCIĄ AUTOMATU - SLIDER
                AppSlider {
                    id: automatonSizeSlider

                    height: 30
                    Layout.fillWidth: true

                    stepSize: 1.0
                    to: 100.0
                    from: 5.0
                    value: 5.0
                    live: false
                    snapMode: Slider.SnapAlways

                    onValueChanged: {
                            automatonSizeText.text = parseInt(value).toString()
                            automatonContainer.getAutomaton().n = value
                            automatonContainer.start()
                    }
                }
                // <- STEROWANIE WIELKOŚCIĄ AUTOMATU - SLIDER
            }
        }
        // <- STEROWANIE WIELKOŚCIĄ AUTOMATU


        // -> STEROWANIE ZOOM'EM AUTOMATU
        AppGroupBox {
            id: automatonZoomGruopBox

            label : Text {
                text: "Zoom"
                color: "#fff"
            }

            AppSpinBox {
                from: 1
                to: 10
                stepSize: 1

                onValueChanged: {
                    automatonContainer.getAutomaton().zoom = 1 + (value / 5.0 - 0.2);
                    automatonContainer.getAutomaton().calculate();
                    automatonContainer.getAutomatonGraphics().draw();
                }
            }
        }
        // <- STEROWANIE ZOOM'EM AUTOMATU
    }
    // <- WIERSZ WIELKOŚCI, ZOOM'U


    // -> WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU
    RowLayout {
        id: automatonParamsRow

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: automatonSizeRowLayout.bottom
        anchors.topMargin: 10

        // -> WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - PERIODYCZNOIŚĆ
        AppGroupBox {
            id: automatonPeriodicityGruopBox

            Layout.fillWidth: true

            label : Text {
                text: "Periodyczność"
                color: "#fff"
            }

            AppSwitch {
                onCheckedChanged: {
                    automatonContainer.getAutomatonLogics().setPeriodicity(checked)
                }
            }
        }
        // <- WWIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - PERIODYCZNOIŚĆ


        // -> WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - SĄSIEDZTWO
        AppGroupBox {
            id: automatonNeighbourhoodGruopBox
            Layout.fillWidth: true

            label : Text {
                text: "Sąsiedztwo"
                color: "#fff"
            }

            RowLayout {
                AppRadioButton {
                    id: mooreRadioBox
                    text: "Moore"
                }

                AppRadioButton {
                    id: neumannRadioBox
                    text: "Neumann"

                    checked: true
                    onCheckedChanged: {
                        if (checked)
                            automatonContainer.getAutomatonLogics().setNeighbourhood(4);
                        else
                            automatonContainer.getAutomatonLogics().setNeighbourhood(8);
                    }
                }
            }
        }
        // <- WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - SĄSIEDZTWO


        // -> WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - STEROWANIE INTERWAŁEM AUTOMATU
        AppGroupBox {
            id: automatonIntervalGroupBox

            label : Text {
                text: "Interwał"
                color: "#fff"
            }

            AppSpinBox {
                from: 100
                to: 10000
                stepSize: 100

                onValueChanged: {
                    automatonContainer.stop()
                    automatonContainer.timer.interval = value
                    automatonContainer.start()
                }
            }
        }
        // <- WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU - STEROWANIE INTERWAŁEM AUTOMATU
    }
    // <- WIERSZ PERIODYCZNOŚCI ORAZ SĄSIEDZTWA ORAZ INTERWAŁU


    // -> WIERSZ ITERACJI
    AppGroupBox {
        id: iteratorsGroupBox

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: automatonParamsRow.bottom
        anchors.topMargin: 10

        label : Text {
            text: "Iteracje"
            color: "#fff"
        }

        RowLayout {
            id: iterationsSwitch
            height: 40
            anchors.fill: parent
            spacing: 10

            // -> WIERSZ ITERACJI - WŁĄCZNIK
            AppSwitch {
                onCheckedChanged: {
                    (checked) ?
                        automatonContainer.getAutomatonIterator().enable() :
                        automatonContainer.getAutomatonIterator().disable();
                }
            }
            // <- WIERSZ ITERACJI - WŁĄCZNIK


            // -> WIERSZ ITERACJI -  PRZYCISKI STERUJĄCE
            Row {
                spacing: 5
                Layout.fillWidth: true

                Row {
                    Image {
                        id: iterationsFirstButton
                        width: 30
                        height: 30
                        source:
                            (iterationsFirstMouseArea.containsMouse) ?
                                 "iterator-icons/first_a.png" : "iterator-icons/first.png"

                        MouseArea {
                            id: iterationsFirstMouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: {
                                automatonContainer.getAutomatonIterator().firstIteration();
                            }
                        }
                    }

                    Image {
                        id: iterationsBackButton
                        width: 30
                        height: 30
                        source:
                            (iteratorsBackMouseArea.containsMouse) ?
                                 "iterator-icons/back_a.png" : "iterator-icons/back.png"

                        MouseArea {
                            id: iteratorsBackMouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: {
                                automatonContainer.getAutomatonIterator().prevIteration();
                            }
                        }
                    }

                    Image {
                        id: iterationsPlayButton
                        width: 30
                        height: 30
                        source:
                            if (iterationsPlayButtonMouseArea.containsMouse)
                                (automatonContainer.iteratorTimer.running ? "iterator-icons/stop_a.png" : "iterator-icons/play_a.png")
                            else
                                (automatonContainer.iteratorTimer.running ? "iterator-icons/stop.png" : "iterator-icons/play.png")

                        MouseArea {
                            id: iterationsPlayButtonMouseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                automatonContainer.iteratorTimer.running ?
                                    automatonContainer.iteratorTimer.stop() :
                                    automatonContainer.iteratorTimer.start();
                            }
                        }
                    }

                    Image {
                        id: iterationsNextButton
                        width: 30
                        height: 30
                        source:
                            (iteratorsNextMouseArea.containsMouse) ?
                                 "iterator-icons/next_a.png" : "iterator-icons/next.png"

                        MouseArea {
                            id: iteratorsNextMouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: {
                                automatonContainer.getAutomatonIterator().nextIteration();
                            }
                        }
                    }

                    Image {
                        id: iterationsLastButton
                        width: 30
                        height: 30
                        source:
                            (iteratorsLastMouseArea.containsMouse) ?
                                 "iterator-icons/last_a.png" : "iterator-icons/last.png"

                        MouseArea {
                            id: iteratorsLastMouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: {
                                automatonContainer.getAutomatonIterator().lastIteration();
                            }
                        }
                    }
                }
            }
            // <- WIERSZ ITERACJI - PRZYCISKI STERUJĄCE


            // -> WIERSZ ITERACJI - POLE LICZBY ITERACJI
            AppText {
                text: "Liczba: "
            }

            AppTextField {
                id: iterationsText
                width: 50
                Layout.preferredWidth: 50
                text: "0"
            }
            // <- WIERSZ ITERACJI - POLE LICZBY ITERACJI
        }
    }
    // <- WIERSZ ITERACJI


    // -> TABELA WARTOŚCI PARAMETRÓW KOMÓREK
    AppGroupBox {
        id: cellGroupBox

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: iteratorsGroupBox.bottom
        anchors.topMargin: 10
        anchors.bottom: buttonsRect.top
        anchors.bottomMargin: 20

        label : Text {
            text: "Parametry komórek"
            color: "#fff"
        }

        ScrollView {
            id: scrollView
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.fill: parent
            contentWidth: width
            clip: true


            CellValues {
                id: cellValues

                width: parent.width
                anchors.top: parent.top
            }

        }
    }
    // <- TABELA WARTOŚCI PARAMETRÓW KOMÓREK


    // -> WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM
    RowLayout {
        id: buttonsRect

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        // -> WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - ZASTOSUJ DO WSZYSTKICH
        AppButton {
            height: 40
            text: "Zastosuj do wszystkich"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: randAutomatonButton.left
            anchors.rightMargin: 10

            onClicked: {
                automatonContainer.getAutomaton().setAllCells(automatonContainer.getAutomatonGraphics().getSelectedCell());
                automatonContainer.getAutomatonGraphics().draw();
            }
        }
        // <- WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - ZASTOSUJ DO WSZYSTKICH


        // -> WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - INICJUJ WARTOŚCI
        AppButton {
            id: randAutomatonButton
            height: 40
            text: "Inicjuj wartości"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: toggleAutomatonButton.left
            anchors.rightMargin: 10

            onClicked: {
                automatonContainer.start()
            }
        }
        // <- WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - INICJUJ WARTOŚCI


        // -> WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - URUCHOM / ZATRZYMAJ
        AppButton {
            id: toggleAutomatonButton
            height: 40
            text: automatonContainer.timer.running ? "Zatrzymaj automat" : "Uruchom automat"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right

            onClicked: {
                if (automatonContainer.timer.running) {
                    automatonContainer.timer.stop();
                }
                else {
                    automatonContainer.getAutomatonIterator().reset();
                    automatonContainer.timer.start();
                }
            }
        }
        // <- WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM - URUCHOM / ZATRZYMAJ
    }
    // <- WIERSZ PRZYCISKÓW STERUJĄCYCH AUTOMATEM
}
