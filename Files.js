/*!
  \fn Files.readFile(url : String) : String

  Funkcja czyta plik o położony w \a url i zwraca jego zawartość.
  */
function readFile(url)
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);
    xhr.send(null);

    return xhr.responseText;
}

/*!
  \fn Files.saveFile(url : String, text: String) : void

  Funkcja zapisuje do pliku o położeniu \a url tekst zapisany w \a tekst,
  kasując poprzednią zawartośc pliku.
  Jeśli plik nie istnieje - jest tworzony.
  */
function saveFile(url, text)
{
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url, false);
    xhr.send(text);
}

/*!
  \fn Files.readCharTable(void) : Array(String[], float[])

  Funkcja odczytuje z pliku tekstowego zestaw znaków i
  zwraca tablicę tych znaków. Wykorzystywana jest w GUI do wyboru znaku,
  który użytkownik może wybrać jako parametr komórki.
  */
function readCharTable() {
    try {
        var table = JSON.parse(readFile("char-table.txt"));

        if (!Array.isArray(table))
            throw "Err";

        for (var i = 0; i < table.length; ++i)
            if (Array.isArray(table[i])) {
                if (table[i].length !== 2)
                    throw "Err";
            }
            else
                throw "Err";
    }
    catch (e) {
        console.log("Niepoprawna tablica!");
    }

    return table;
}

/*!
  \fn Files.isIterationsArray(object : Object) : bool

  Funkcja sprawdza czy obiekt \a object jest poprawną tablicą iteracji.
  */
function isIterationsArray(object) {
    if (Array.isArray(object)) {
        for (var i = 0; i < object.length; ++i) {
            if (!isCellsArray(object[i]))
                return false;
        }

        return true;
    }

    return false;
}

/*!
  \fn Files.isCellsArray(object : Object) : bool

  Funkcja sprawdza czy obiekt \a object jest poprawną tablicą komórek.
  */
function isCellsArray(object) {
    //Czy to w ogóle tablica?
    if (Array.isArray(object)) {
        //Minimalny rozmiar automatu to 5x5
        if (object.length < 5)
            return false;

        //To musi być tablica 2-wymiarowa
        for (var i = 0; i < object.length; ++i)
            if (!Array.isArray(object[i])) {
                return false;
            }

        //To musi być tablica kwadratowa
        for (var i = 0; i < object.length; ++i)
            if (object[i].length !== object.length)
                return false;

        //Czy każda komórka jest poprawnym obiektem?
        for (var i = 0; i < object.length; ++i)
            for (var j = 0; j < object[i].length; ++j) {
                var c = object[i][j];

                if (typeof c.r === "undefined" || typeof c.g === "undefined" ||
                    typeof c.b === "undefined" || typeof c.border === "undefined" ||
                    typeof c.character === "undefined" || typeof c.state === "undefined" ||
                    typeof c.add === "undefined"
                   )
                    return false;

                //Czy wszystkie wartości to float'y?
                if (Number(c.r) !== c.r && c.r !== null)
                    return false;
                if (Number(c.g) !== c.g && c.g !== null)
                    return false;
                if (Number(c.b) !== c.b && c.b !== null)
                    return false;
                if (Number(c.border) !== c.border && c.border !== null)
                    return false;
                if (Number(c.character) !== c.character && c.character !== null)
                    return false;
                if (Number(c.state) !== c.state && c.state !== null)
                    return false;
            }


    }
    else
        return false;

    return true;
}


/*!
  \fn Files.deserialize(jsonString : String) : void, throws SyntaxError exception

  Funkcja konwertuje \a jsonString na tablicę komórek, sprawdzając czy jest to w ogóle możliwe.
  W przypadku błędu w formacie JSON wyrzuca wyjątek SyntaxError.
  */
function deserialize(jsonString) {
    try {
        var object = JSON.parse(jsonString);

        if (isCellsArray(object))
            automatonContainer.setCells(object);
   }
   catch (e) {
       console.log(e);
   }
}

/*!
  \fn Files.deserializeIterations(jsonString : String) : void, throws SyntaxError exception

  Funkcja konwertuje \a jsonString na tablicę iteracji, sprawdzając czy jest to w ogóle możliwe.
  W przypadku błędu w formacie JSON wyrzuca wyjątek SyntaxError.
  */
function deserializeIterations(jsonString) {
    try {
        var object = JSON.parse(jsonString);

        if (isIterationsArray(object))
            automatonContainer.setIterations(object);
   }
   catch (e) {
       console.log(e);
   }
}

/*!
  \fn Files.runLoadedScript(url : String) : void

  Funkcja uruchamia skrypt automatu wczytany z pliku \a url.
  */
function runLoadedScript(url) {
    Qt.include(url);

    automatonContainer.getAutomatonScript().url = url;
    automatonContainer.getAutomatonScript().init = init;
    automatonContainer.getAutomatonScript().update = update;

    automatonContainer.start();
}
