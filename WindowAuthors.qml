﻿import QtQuick 2.8
import QtQuick.Layouts 1.3

/*!
  \qmltype WindowAuthors

  Okno prezentujące autorów projektu.
  */
Rectangle {
    id: authorsRect
    color: "#88000000"
    visible: false
    anchors.fill: parent

    // -> OKNO
    Rectangle {
        width: 600
        height: 400
        color: "#eee"
        anchors.centerIn: parent

        // -> OKNO - TYTUŁ
        Text {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.leftMargin: 10
            width: 200
            height: 20
            verticalAlignment: Text.AlignVCenter
            text: "Autorzy projektu"
        }
        // <- OKNO - TYTUŁ


        // -> OKNO - PRZYCISK ZAMYKANIA
        Image {
            id: closeButton
            width: 20
            height: 20
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 10
            source: "img/close.png"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: authorsRect.visible = false;
            }
        }
        // <- OKNO - PRZYCISK ZAMYKANIA


        // -> OKNO - LISTA AUTORÓW
        ColumnLayout {

            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: closeButton.bottom
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 40
            spacing: 16

            ColumnLayout {
                Text {
                    text: "Mariusz Niedziółka"
                    font.bold: Font.Bold
                }
                Text {
                    text: "szef projektu, projekt GUI, główny obiekt automatu, iterator automatu"
                }
           }

            ColumnLayout {
                Text {
                    text: "Mateusz Lewandowski"
                    font.bold: Font.Bold
                }
                Text {
                    text: "system wejścia/wyjścia do plików, sprawdzanie poprawności wczytywanych danych"
                }
           }

            ColumnLayout {
                Text {
                    text: "Dawid Maciak"
                    font.bold: Font.Bold
                }
                Text {
                    text: "tworzenie dokumentacji oraz podręcznika użytkownika"
                }
           }

            ColumnLayout {
                Text {
                    text: "Piotr Mazur"
                    font.bold: Font.Bold
                }
                Text {
                    text: "skrypty GUI, graficzny obiekt automatu"
                }
           }

            ColumnLayout {
                Text {
                    text: "Michał Słotwiński"
                    font.bold: Font.Bold
                }
                Text {
                    text: "logika i skrypty automatu, pomysłodawca reguł sterujących automatem"
                }
           }
        }
        // <- OKNO - LISTA AUTORÓW
    }
    // <- OKNO
}
