import QtQuick 2.8
import QtQuick.Controls 2.2

import "RectAutoSize.js" as RectAoutSizeScript

ApplicationWindow {
    visible: true

    //Domyślny rozmiar okna
    width: 1280
    height: 720

    //Nazwa aplikacji
    title: qsTr("Cell")

    //Eventy dostosowujące rozmiar elementów GUI w zależności od rozmiaru okna.
    onHeightChanged: {
        mainForm.automatonContainer.getAutomaton().calculate();
    }
    onWidthChanged: {
        mainForm.automatonContainer.getAutomaton().calculate();
    }

    MainForm {
        id: mainForm
    }
}
