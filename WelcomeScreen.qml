import QtQuick 2.8
import "RectAutoSize.js" as RectAoutSizeScript
import "Files.js" as FilesScript

/*!
  \qmltype WelcomeScreen

  Ekran powitalny aplikacji. Wyświetla się dopóki GUI nie zostanie w pełni załadowany.
  */
Rectangle {
    id: welcomeScreen
    anchors.fill: parent
    color: "#222222"

    // -> ANIMACJA UKRYWAJĄCA OKNO
    NumberAnimation on opacity {
        id: welcomeScreenAnimation
        from: 1.0
        to: 0.0
        duration: 1600
        running: false
    }
    // <- ANIMACJA UKRYWAJĄCA OKNO


    // -> TIMER AKTYWUJĄCY ANIMACJĘ
    Timer {
        id: welcomeScreenTimer
        interval: 2000
        repeat: false
        onTriggered: {
            charTable.updateChars(FilesScript.readCharTable())
            welcomeScreenAnimation.start()
            automatonContainer.init()
        }
    }
    // <- TIMER AKTYWUJĄCY ANIMACJĘ


    // -> LOGO
    Image {
        width: 250
        height: 250
        anchors.centerIn: parent
        source: "img/icon.png"
        fillMode: Image.PreserveAspectCrop
    }
    // <- LOGO

    //Gdy komponent się załaduje:
    Component.onCompleted: {
        welcomeScreenTimer.start()
    }
}
