Qt.include("Files.js")

//Obiekt reprezentujący interfejs do obsługi skryptu automatu.
var automatonScript;

/*!
  \fn AutomatonScript.createAutomatonScript(automatonLogics: AutomatonLogics) : void

  Funkcja tworząca instancję obiektu odpowiedzialnego za uruchamianie i
  sterowanie skryptem automatu.
  */
function createAutomatonScript(automatonLogics){
    automatonScript = new AutomatonScript(automatonLogics);
}

/*!
  \qmltype AutomatonScript

  Klasa służąca do obsługi skryptu automatu. Udostępnia domyślne metody
  inicjalizacji komórek oraz aktualizacji stanu automatu. Metody inicjalizacji
  i aktualizacji stanu są domyślnie wykonywane, dopóki nie zostanie załadowany skrypt
  z zewnętrznego pliku JavaScript. Konstruktor pobiera referencję do obiektu odpowiedzialnego
  za logikę automatu.
  */
function AutomatonScript(automatonLogics)
{
    //Referencje do obiektów automatu
    this.url = "";
    this.scriptObject = null;

    this.automaton = automatonLogics.automaton;
    this.automatonGraphics = automatonContainer.getAutomatonGraphics();
    this.automatonLogics = automatonLogics;

    //Referencje do zmiennych i obiektów
    this.getN = function() { return this.automaton.n; }
    this.getCell = function(i, j) { return this.automaton.cells[i][j]; };

    //Referencje do funkcji
    this.getNeighbours = function(i, j) {return this.automatonLogics.getNeighbours(i, j);};
    this.get = function(cell, param) {return this.automaton.getCellValue(cell, param); };
    this.set = function(cell, param, value) {this.automaton.setCellValue(cell, param, value);};
    this.hsvToRgb = function(hsv) { return this.automatonGraphics.hsvToRgb(hsv); };
    this.rgbToHsv = function(rgb) { return this.automatonGraphics.rgbToHsv(rgb); };
    this.setNeighbourhood = function(count) { this.automatonLogics.setNeighbourhood(count); };
}

/*!
    \fn AutomatonScript.init(void) : void

    Funkcja inicjalizująca wartości komórek według ustalonych reguł.
    W swojej domyślnej wersji inicjuje komórki kolorem czarnym bądź białym.
  */
AutomatonScript.prototype.init = function() {

}

/*!
  \fn AutomatonScript.update(void) : void

  Funkcja aktualizująca stan automatu według wyznaczonych reguł.
  Dla każdej komórki wykonuje obliczenia opisane w skrypcie i odpowiednio zmienia jej stan.
  W swojej domyślej wersji steruje automatem na zasadzie Gry o życie.
  */
AutomatonScript.prototype.update = function() {

}
