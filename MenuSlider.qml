import QtQuick 2.8

/*!
  \qmltype MenuSlider

  Jest to ozdobny, animowany pasek wyświetlający się pod przyciskami menu.
  */
Rectangle {
    property alias menuSlider: menuSlider
    property alias menuSliderAnimation: menuSliderAnimation
    property alias menuSliderWidthAnimation: menuSliderWidthAnimation

    id: menuSlider
    width: 0
    height: 2
    x: 0
    color: "#349487"
    anchors.top: menu.bottom
    anchors.topMargin: -2

    //Animacje wywołane najechamiem bądź wyjechamiem kursora myszy z przycisku menu.
    NumberAnimation on x {
        property int slideFrom : 0
        property int slideTo : -100

        id: menuSliderAnimation
        from: slideFrom
        to: slideTo
        duration: 500
        alwaysRunToEnd: false
        running: false
        easing.type: Easing.InOutQuad
    }

    NumberAnimation on width {
        property int slideFrom : 0
        property int slideTo : -100

        id: menuSliderWidthAnimation
        from: slideFrom
        to: slideTo
        duration: 500
        alwaysRunToEnd: false
        running: false
        easing.type: Easing.InOutQuad
    }
}
